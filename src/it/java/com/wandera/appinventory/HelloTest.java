package com.wandera.appinventory;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.google.inject.testing.fieldbinder.Bind;
import com.wandera.commons.service.rabbitmq.consumer.Consumer;
import com.wandera.commons.service.rabbitmq.producer.Producer;

// TODO: change
public class HelloTest extends IntegrationTest {

	@Mock
	@Bind
	Producer producer;

	@Mock
	@Bind
	Consumer consumer;

	@Before
	public void setup() throws IOException {
	}

	@Test
	public void testHello() {
		Response response = client.target(uriBuilder("/hello-world"))
				.request(MediaType.APPLICATION_JSON)
				.get();

		assertThat(response.getStatusInfo(), is(Response.Status.OK));
	}
}
