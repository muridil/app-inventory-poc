package com.wandera.appinventory;

import java.util.Collections;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.JerseyClientBuilder;
import org.junit.After;
import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.google.inject.Module;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import com.wandera.commons.service.auth.mongo.MongoAuthorizer;
import com.wandera.commons.service.mongo.SpringMongoManager;
import com.wandera.commons.service.mongo.test.MongoTest;
import com.wandera.appinventory.impl.Test;
import com.wandera.appinventory.impl.TestConfiguration;

import io.dropwizard.testing.ConfigOverride;
import io.dropwizard.testing.DropwizardTestSupport;
import io.dropwizard.testing.ResourceHelpers;

public abstract class IntegrationTest extends MongoTest {

	@Bind
	SpringMongoManager springMongoManagerBind = springMongoManager;

	@Bind
	@Mock
	MongoAuthorizer authorizer;

	Client client = new JerseyClientBuilder().build();

	DropwizardTestSupport<AppInventoryConfiguration> testSupport;

	@Before
	public void setupIntegrationTest() {
		MockitoAnnotations.initMocks(this);

		System.setProperty("mongo.uri", "mongodb://localhost/it");
		System.setProperty("graphite.host", "http://localhost/it");

		testSupport = new DropwizardTestSupport<AppInventoryConfiguration>(
				AppInventory.class,
		ResourceHelpers.resourceFilePath("app-inventory.yaml"),
				ConfigOverride.config("logging.level", "DEBUG"),
				ConfigOverride.config("logging.appenders[0].threshold", "OFF"),
				ConfigOverride.config("logging.appenders[1].threshold", "ALL"),
				ConfigOverride.config("security.signatureEnabled", "false"),
				ConfigOverride.config("security.authorizationEnabled", "false")) {

			@Override
			public AppInventory newApplication() {
				return new AppInventory() {
					@Override
					public List<Module> getModules() {
						Module module = Modules.override(super.getModules())
								.with(BoundFieldModule.of(IntegrationTest.this));
						return Collections.singletonList(module);
					}
				};
			}
		};
		testSupport.before();
	}

	@After
	public void tearDownIntegrationTest() {
		testSupport.after();
	}

	protected UriBuilder uriBuilder(String basePath) {
		return UriBuilder.fromUri(String.format("http://localhost:%d%s", testSupport.getLocalPort(), basePath));
	}
}