#!/bin/bash
echo $APP_INVENTORY_HOME
if [ -z "$APP_INVENTORY_HOME" ]; then
        echo "AppInventory service home not set, using current directory as default. if this is not correct set the $APP_INVENTORY_HOME variable"
        HOME=`pwd`
else
        echo "State analyser service home is set to $APP_INVENTORY_HOME"
        HOME=$APP_INVENTORY_HOME
fi

echo "Running AppInventory service in: $HOME"

cd $HOME
LIB=$HOME/lib
JAR_NAME=`find $LIB -name app-inventory*.jar`
ETC=$HOME/etc

# Set the maximum heap the process should use. A heap that is too large creates large GCs and potential blocking or high cpu usage dying this time.
JAVA_OPTS="-Xmx64m"

# Create a heap dump we can analyse when we run out of memory
JAVA_OPTS="$JAVA_OPTS -XX:+HeapDumpOnOutOfMemoryError"

# Path that the heap dump should be created in.
JAVA_OPTS="$JAVA_OPTS -XX:HeapDumpPath=$HOME/dump/"

# Use G1 garbage collector
JAVA_OPTS="$JAVA_OPTS -XX:+UseG1GC"

nohup $JAVA_HOME/bin/java $JAVA_OPTS -jar $JAR_NAME server \
        -p /opt/service-properties/environment.properties \
        -p /opt/service-properties/app-inventory.properties \
        -y $ETC/app-inventory.yaml \
        > nohup.out 2>&1 &