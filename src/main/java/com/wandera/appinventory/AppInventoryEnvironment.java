package com.wandera.appinventory;

import javax.inject.Inject;

import io.dropwizard.lifecycle.Managed;

public class AppInventoryEnvironment implements Managed {

	private final AppInventoryConfiguration configuration;

	@Inject
	public AppInventoryEnvironment(AppInventoryConfiguration configuration) {

		this.configuration = configuration;
	}

	@Override
	public void start() throws Exception {
	}

	@Override
	public void stop() throws Exception {
	}
}