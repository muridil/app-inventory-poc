package com.wandera.appinventory;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.inject.Module;
import com.wandera.appinventory.module.KStreamsTopologyModule;
import com.wandera.commons.service.dropwizard.AbstractWanderaApplication;

import io.dropwizard.setup.Environment;

public class AppInventory extends AbstractWanderaApplication<AppInventoryConfiguration> {

	public static final String SERVICE_NAME = "APP_INVENTORY";

	public static void main(String[] args) throws Exception {
		new AppInventory().run(args);
	}

	@Override
	public String getName() {
		return SERVICE_NAME;
	}

	@Override
	protected List<Module> getModules() {
		return ImmutableList.of(
				new KStreamsTopologyModule());
	}

	@Override
	protected void doRun(AppInventoryConfiguration configuration, Environment environment) {
	}

}