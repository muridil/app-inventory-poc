package com.wandera.appinventory;

import java.nio.charset.Charset;
import java.util.Map;

import org.apache.kafka.common.serialization.Serializer;

import com.google.gson.Gson;

public class JsonSerializer<T> implements Serializer<T> {

	/* Members */
	private Gson gson = new Gson();

	/**
	 * Override function that is kept empty.
	 * 
	 * @param map
	 * @param b
	 */
	@Override
	public void configure(Map<String, ?> map, boolean b) {
	}

	/**
	 * Serialize the data into a byte array.
	 * 
	 * @param topic
	 * @param t
	 * @return
	 */
	@Override
	public byte[] serialize(String topic, T t) {
		return gson.toJson(t).getBytes(Charset.forName("UTF-8"));
	}

	/**
	 * Override function that is kept empty.
	 */
	@Override
	public void close() {
	}
}