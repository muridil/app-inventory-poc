package com.wandera.appinventory;

import java.util.Map;

import org.apache.kafka.common.serialization.Deserializer;

import com.google.gson.Gson;

public class JsonDeserializer<T> implements Deserializer<T> {

	/* Members */
	private Gson gson = new Gson();
	private Class<T> deserializedClass;

	/**
	 * Constructor. Receives the type we are de-serializing.
	 * 
	 * @param deserializedClass
	 */
	public JsonDeserializer(Class<T> deserializedClass) {

		this.deserializedClass = deserializedClass;
	}

	/**
	 * Constructor.
	 */
	public JsonDeserializer() {
	}

	@Override
	@SuppressWarnings("unchecked")
	/**
	 * Sets the de-serialized class if none was provided in the constructor.
	 */
	public void configure(Map<String, ?> map, boolean b) {
		if (deserializedClass == null) {
			deserializedClass = (Class<T>) map.get("serializedClass");
		}
	}

	@Override
	/**
	 * De-serializes a specific string to the class we want it to be.
	 */
	public T deserialize(String s, byte[] bytes) {
		if (bytes == null) {
			return null;
		}

		try {
			return gson.fromJson(new String(bytes), deserializedClass);
		} catch (Exception ex) {
			System.out.println("Exception occurred while trying to deserialize string s: " + s + ", with byte array. "
					+ ex.getStackTrace());
			return null;
		}

	}

	@Override
	/**
	 * Does nothing ATM.
	 */
	public void close() {

	}
}