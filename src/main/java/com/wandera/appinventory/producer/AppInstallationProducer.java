package com.wandera.appinventory.producer;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wandera.appinventory.AppInventory;
import com.wandera.appinventory.JsonSerializer;
import com.wandera.appinventory.core.AppInstallation;

public class AppInstallationProducer {

	private static final Logger LOGGER = LoggerFactory.getLogger(AppInventory.class);
	private static final String APP_INSTALATIONS_TOPIC = "app-installations-input-topic";

	public static void main(String[] args) {
		final int devices = 100 * 1000;
		final int applications = 300;
		final int events = 100 * 1000 * 1000;
		final Random rg = new Random();
		final Properties producerConfig = new Properties();
		producerConfig.put("bootstrap.servers", "localhost:9092");

		List<String> guids = new ArrayList<>(devices);
		for (int i = 0; i < devices; i++) {
			guids.add("device-" + i);
		}
		
		try (KafkaProducer<String, AppInstallation> kafkaProducer = new KafkaProducer<>(producerConfig,
				new StringSerializer(), new JsonSerializer<>());) {
			for (int i = 0; i < events; i++) {
				String guid = guids.get(rg.nextInt(devices));
				AppInstallation appInstallation = new AppInstallation("app-" + rg.nextInt(applications), "x.y.z");

				kafkaProducer.send(new ProducerRecord<>(APP_INSTALATIONS_TOPIC, guid, appInstallation));

//				try {
//					Thread.sleep(1L);
//				} catch (InterruptedException e) {
//					LOGGER.error("oops", e);
//				}
			}
		}
	}
}
