package com.wandera.appinventory.resource;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;

import com.codahale.metrics.annotation.Timed;
import com.wandera.appinventory.core.DeviceAppInventory;

@Path("/v1/app-report")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AppReportResource {

	private final ReadOnlyKeyValueStore<String, DeviceAppInventory> inventoryPerDeviceStore;

	@Inject
	public AppReportResource(KafkaStreams kafkaStreams) {
		inventoryPerDeviceStore = kafkaStreams.store("inventoryPerDevice", QueryableStoreTypes.keyValueStore());
	}

	@GET
	@Timed
	public Response getReport() {
		Map<String, Integer> appsPerDevice = new LinkedHashMap<>();

		KeyValueIterator<String, DeviceAppInventory> range = inventoryPerDeviceStore.all();
		while (range.hasNext()) {
			KeyValue<String, DeviceAppInventory> next = range.next();
			appsPerDevice.put(next.key, next.value.getApps().size());
		}

		return Response.ok(appsPerDevice.entrySet().stream()
				.sorted(Map.Entry.<String, Integer> comparingByValue().reversed())
				.limit(100)
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (x, y) -> y, LinkedHashMap::new)))
				.build();
	}

	@GET
	@Path("/summary")
	@Timed
	public Response getSummary() {
		int values = 0;
		KeyValueIterator<String, DeviceAppInventory> range = inventoryPerDeviceStore.all();

		while (range.hasNext()) {
			range.next();
			values++;
		}

		return Response.ok(values).build();
	}
}
