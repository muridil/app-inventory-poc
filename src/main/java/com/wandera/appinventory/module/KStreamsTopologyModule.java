package com.wandera.appinventory.module;

import java.util.Properties;

import javax.inject.Singleton;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;
import org.apache.kafka.streams.kstream.KTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.wandera.appinventory.AppInventory;
import com.wandera.appinventory.JsonDeserializer;
import com.wandera.appinventory.JsonSerializer;
import com.wandera.appinventory.core.AppInstallation;
import com.wandera.appinventory.core.DeviceAppInventory;

public class KStreamsTopologyModule extends AbstractModule {
	private static final Logger LOGGER = LoggerFactory.getLogger(AppInventory.class);

	@Override
	protected void configure() {
	}

	@Provides
	@Singleton
	public KafkaStreams kafkaStreams() {
		Properties config = new Properties();

		config.put(StreamsConfig.APPLICATION_ID_CONFIG,
				"table-join-kafka-streams");
		config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,
				"localhost:9092");
		config.put(StreamsConfig.KEY_SERDE_CLASS_CONFIG,
				Serdes.String().getClass().getName());
		config.put(StreamsConfig.VALUE_SERDE_CLASS_CONFIG,
				Serdes.String().getClass().getName());
		// 0 MB cache size
		config.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);

		Serde<AppInstallation> appInstallationSerde = Serdes.serdeFrom(new JsonSerializer<>(),
				new JsonDeserializer<>(AppInstallation.class));
		Serde<DeviceAppInventory> deviceAppInventorySerde = Serdes.serdeFrom(new JsonSerializer<>(),
				new JsonDeserializer<>(DeviceAppInventory.class));

		KStreamBuilder builder = new KStreamBuilder();
		
		KStream<String, AppInstallation> appInstallations = builder.stream(Serdes.String(),
				appInstallationSerde,
				"app-installations-input-topic");

		KTable<String, DeviceAppInventory> inventoryPerDevice = builder.table(Serdes.String(),
				deviceAppInventorySerde,
				"inventory-per-device-topic", "inventoryPerDevice");

		appInstallations.leftJoin(inventoryPerDevice, (app, inventory) -> {
			if (inventory == null) {
				inventory = new DeviceAppInventory();
			}
			inventory.installApp(app);
			return inventory;
		}, Serdes.String(), appInstallationSerde).to(Serdes.String(),
				deviceAppInventorySerde,
				"inventory-per-device-topic");

		inventoryPerDevice.toStream().foreach((guid, apps) -> {
			LOGGER.debug("App inventory changed for device {}, current state is {} apps installed", guid,
					apps.getApps().size());
		});

		KafkaStreams streams = new KafkaStreams(builder, config);
		streams.start();
		return streams;
	}
}
