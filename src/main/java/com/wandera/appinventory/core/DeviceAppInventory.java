package com.wandera.appinventory.core;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import com.wandera.commons.service.logging.ToStringHelper;

public class DeviceAppInventory {

	private Set<AppInstallation> apps = new HashSet<>();

	public Set<AppInstallation> getApps() {
		return apps;
	}

	public void installApp(AppInstallation appInstallation) {
		apps.add(appInstallation);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		DeviceAppInventory that = (DeviceAppInventory) o;

		return Objects.equals(apps, that.apps);
	}

	@Override
	public int hashCode() {
		return Objects.hash(apps);
	}

	@Override
	public String toString() {
		return ToStringHelper.asString(this);
	}
}
