package com.wandera.appinventory.core;

import java.util.Objects;

import com.wandera.commons.service.logging.ToStringHelper;

public class AppInstallation {

	private final String appName;
	private final String version;

	public AppInstallation(String appName, String version) {
		this.appName = appName;
		this.version = version;
	}

	public String getAppName() {
		return appName;
	}

	public String getVersion() {
		return version;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		AppInstallation that = (AppInstallation) o;

		return Objects.equals(appName, that.appName) && Objects.equals(version, that.version);
	}

	@Override
	public int hashCode() {
		return Objects.hash(appName, version);
	}

	@Override
	public String toString() {
		return ToStringHelper.asString(this);
	}
}
