# AppInventory

* port: 1999

* How to run locally: ```server -y src/main/resources/app-inventory.yaml -p src/test/resources/environment.properties src/test/resources/app-inventory.properties```

You need running Kafka on its standard ports:
```docker run -p 2181:2181 -p 9092:9092 --env ADVERTISED_HOST=localhost  --env ADVERTISED_PORT=9092 spotify/kafka```
